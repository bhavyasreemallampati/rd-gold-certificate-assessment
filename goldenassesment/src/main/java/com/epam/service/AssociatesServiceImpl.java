package com.epam.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.dto.AssociatesDto;
import com.epam.entity.Associates;
import com.epam.exception.AssociatesException;
import com.epam.repository.AssociatesRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AssociatesServiceImpl implements AssociatesService {
	@Autowired
	AssociatesRepository associatesRepository;
	@Autowired
	ModelMapper modelMapper;

	@Override
	public AssociatesDto addAssociate(AssociatesDto associatesDto) {
		log.info("Entered into add associate of service");
		Associates associates = Optional
				.ofNullable(associatesRepository.save(modelMapper.map(associatesDto, Associates.class)))
				.orElseThrow(() -> new AssociatesException("The feilds cannot be null"));
		log.info("Associate got added{}", associates);
		return modelMapper.map(associates, AssociatesDto.class);
	}

	@Override
	public AssociatesDto updateAssociate(AssociatesDto associatesDto) {
		log.info("Entered into update associate of service");
		Associates associates = Optional
				.ofNullable(associatesRepository.save(modelMapper.map(associatesDto, Associates.class)))
				.orElseThrow(() -> new AssociatesException("The feilds cannot be null"));
		log.info("Associate got updated{}", associates);
		return modelMapper.map(associates, AssociatesDto.class);
	}

	@Override
	public List<AssociatesDto> viewAssociates(String gender) throws AssociatesException {
		log.info("Entered into view associate of service");
		return associatesRepository.findAllByGender(gender).stream()
				.map(associate -> modelMapper.map(associate, AssociatesDto.class)).toList();
	}

	@Override
	public void deleteAssociate(int index) {
		log.info("Entered into delete associate of service");
		associatesRepository.deleteById(index);
		log.info("Associate got deleted at index{}", index);

	}

	

}
