package com.epam.service;

import java.util.List;
import com.epam.dto.AssociatesDto;


public interface AssociatesService {
	public AssociatesDto addAssociate(AssociatesDto associatesDto);
	public AssociatesDto updateAssociate(AssociatesDto associatesDto);
	public List<AssociatesDto> viewAssociates(String gender);
	public void deleteAssociate(int index);

}
