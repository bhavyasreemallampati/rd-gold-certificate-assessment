package com.epam.exceptionHandling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.exception.AssociatesException;

import lombok.extern.slf4j.Slf4j;

@RestControllerAdvice
@Component
@Slf4j
public class RestExceptionHandler {
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionFormat handleMethodArgumentNotValidException(MethodArgumentNotValidException er,
			WebRequest request) {
		List<String> inputError = new ArrayList<>();
		er.getAllErrors().forEach(error -> inputError.add(error.getDefaultMessage()));
		return new ExceptionFormat(new Date().toString(), HttpStatus.BAD_REQUEST.name(),
				inputError.toString(), request.getDescription(false));

	}

	@ExceptionHandler(AssociatesException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ExceptionFormat handleAssociateException(AssociatesException er, WebRequest request) {
		ExceptionFormat exceptionFormat = new ExceptionFormat(new Date().toString(),
				HttpStatus.BAD_REQUEST.name(), er.toString(), request.getDescription(false));
		log.info(er.toString());
		return exceptionFormat;
	}
	@ExceptionHandler(DataIntegrityViolationException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionFormat handleDataIntegrityViolationException(DataIntegrityViolationException er,
			WebRequest request) {
		ExceptionFormat exceptionFormat = new ExceptionFormat(new Date().toString(),
				HttpStatus.INTERNAL_SERVER_ERROR.name(), "Can't delete as id not found", request.getDescription(false));
		log.info(er.toString());
		return exceptionFormat;
	}

	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionFormat handleRuntimeException(RuntimeException er, WebRequest request) {
		ExceptionFormat exceptionFormat = new ExceptionFormat(new Date().toString(),
				HttpStatus.INTERNAL_SERVER_ERROR.name(), er.toString(), request.getDescription(false));
		log.info(er.toString());
		return exceptionFormat;
	}

}
