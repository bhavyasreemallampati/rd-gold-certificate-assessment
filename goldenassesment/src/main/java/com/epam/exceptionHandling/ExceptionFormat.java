package com.epam.exceptionHandling;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExceptionFormat {
	String timestamp;
	String status;
	String error;
	String path;
}
