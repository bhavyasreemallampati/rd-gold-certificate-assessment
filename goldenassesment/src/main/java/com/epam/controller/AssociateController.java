package com.epam.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.AssociatesDto;
import com.epam.service.AssociatesService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("associates")
public class AssociateController {
	@Autowired
	AssociatesService associatesService;
	
	@PostMapping
	public ResponseEntity<AssociatesDto> addAssociate(@RequestBody @Valid AssociatesDto associatesDto){
		log.info("Entered into add associate of rest");
		return new ResponseEntity<>(associatesService.addAssociate(associatesDto), HttpStatus.CREATED);	
	}
	@PutMapping
	public ResponseEntity<AssociatesDto> updateAssociate(@RequestBody @Valid AssociatesDto associatesDto){
		log.info("Entered into update associate of rest");
		return new ResponseEntity<>(associatesService.updateAssociate(associatesDto), HttpStatus.OK);	
	}
	@GetMapping("/{gender}")
	public ResponseEntity<List<AssociatesDto>> getByGender(@PathVariable @Valid String gender){
		log.info("Entered into veiw associate of rest");
		return new ResponseEntity<>(associatesService.viewAssociates(gender),HttpStatus.OK);
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteAssociates(@PathVariable @Valid int id) {
		log.info("Entered into update associate of rest");
		associatesService.deleteAssociate(id);
		return new ResponseEntity<>("Associate with the given index got delete",HttpStatus.NO_CONTENT);
	}
	

}
