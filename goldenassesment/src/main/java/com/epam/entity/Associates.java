package com.epam.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Associates {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "associatesId")
	private int id;
	private String name;
	private String email;
	private String gender;
	private String college;
	private String status;
	@ManyToOne(cascade = { CascadeType.DETACH,CascadeType.ALL,CascadeType.MERGE})
	private Batch batch;

}
