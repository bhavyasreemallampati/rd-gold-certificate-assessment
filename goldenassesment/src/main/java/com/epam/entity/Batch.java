package com.epam.entity;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Batch {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="batchId")
	private int id;
	@Column(unique = true)
	private String name;
	@Column(unique = true)
	private String practice;
	private Date startDate;
	private Date endDate;

}
