package com.epam.dto;


import com.epam.entity.Batch;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AssociatesDto {
		@Schema(accessMode = AccessMode.READ_ONLY)
		int id;
		@NotNull(message="Name cannot be null")
		private String name;
		@Email(message="Enter the email correctly")
		private String email;
		@NotNull(message="gender cannot be null")
		private String gender;
		@NotNull(message="college cannot be null")
		private String college;
		@NotNull(message="status cannot be null")
		private String status;
		private Batch batch;


}
