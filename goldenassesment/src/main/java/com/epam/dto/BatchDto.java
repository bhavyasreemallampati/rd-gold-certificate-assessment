package com.epam.dto;

import java.util.Date;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.NotNull;

public class BatchDto {
	@Schema(accessMode = AccessMode.READ_ONLY)
	int id;
	@NotNull(message="Name cannot be null")
	private String name;
	@NotNull(message="practice cannot be null")
	private String practice;
	@NotNull(message="date cannot be null")
	private Date startDate;
	@NotNull(message="date cannot be null")
	private Date endDate;
}
