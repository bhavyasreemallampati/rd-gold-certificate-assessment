package com.epam.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.dto.AssociatesDto;
import com.epam.entity.Associates;

import jakarta.transaction.Transactional;

@Repository
public interface AssociatesRepository extends JpaRepository<Associates, Integer> {
	 List<Associates> findAllByGender(String gender);
}
