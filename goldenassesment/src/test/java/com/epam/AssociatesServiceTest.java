package com.epam;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.dto.AssociatesDto;
import com.epam.dto.BatchDto;
import com.epam.entity.Associates;
import com.epam.entity.Batch;
import com.epam.exception.AssociatesException;
import com.epam.repository.AssociatesRepository;
import com.epam.service.AssociatesServiceImpl;

@ExtendWith(MockitoExtension.class)
class AssociatesServiceTest {	
	@Mock
	AssociatesRepository associatesRepository;
	@Mock
	ModelMapper modelMapper;
	@InjectMocks
	AssociatesServiceImpl associatesServiceImpl;
	
	AssociatesDto associatesDto;
	Associates associates;
	Batch batch;
	@BeforeEach
	public void setUp() {
		batch=new Batch(1,"javafolks","java",null,null);
		associatesDto=new AssociatesDto(1,"bhvaya","bhavya@email.com","female","vignan","active",batch);
		associates=new Associates(1,"bhvaya","bhavya@email.com","female","vignan","active",null);
	}
	
	@Test
	void testAddAssociateSucess() throws AssociatesException {
		Mockito.when(modelMapper.map(associatesDto, Associates.class)).thenReturn(associates);
		Mockito.when(associatesRepository.save(any(Associates.class))).thenReturn(associates);
		Mockito.when(modelMapper.map(associates, AssociatesDto.class)).thenReturn(associatesDto);
		AssociatesDto result=associatesServiceImpl.addAssociate(associatesDto);
		assertNotNull(result);
		assertEquals(associates.getId(), 1);
		assertEquals(associates.getName(), result.getName());
		assertEquals(associates.getEmail(), result.getEmail());
		assertEquals(associates.getGender(), result.getGender());
		assertEquals(associates.getCollege(), result.getCollege());
		assertEquals(associates.getStatus(), result.getStatus());
		assertEquals(associates.getBatch(), null);
		Mockito.verify(modelMapper).map(associatesDto, Associates.class);
		Mockito.verify(modelMapper).map(associates, AssociatesDto.class);
		Mockito.verify(associatesRepository).save(associates);
	}
	@Test
	void testAddPracticeFail() {
		assertThrows(AssociatesException.class, ()->associatesServiceImpl.addAssociate(null));
	}
	@Test
	void testUpdateAssociateSucess() throws AssociatesException {
		associates.setId(1);
		associates.setName("bhvaya");
		associates.setEmail("bhavya@email.com");
		associates.setGender("female");
		associates.setCollege("vignan");
		associates.setStatus("active");
		associates.setBatch(null);
		Mockito.when(modelMapper.map(associatesDto, Associates.class)).thenReturn(associates);
		Mockito.when(associatesRepository.save(any(Associates.class))).thenReturn(associates);
		Mockito.when(modelMapper.map(associates, AssociatesDto.class)).thenReturn(associatesDto);
		AssociatesDto result=associatesServiceImpl.updateAssociate(associatesDto);
		assertNotNull(result);
		assertEquals(associates.getId(), 1);
		assertEquals(associates.getName(), result.getName());
		assertEquals(associates.getEmail(), result.getEmail());
		assertEquals(associates.getGender(), result.getGender());
		assertEquals(associates.getCollege(), result.getCollege());
		assertEquals(associates.getStatus(), result.getStatus());
		Mockito.verify(modelMapper).map(associatesDto, Associates.class);
		Mockito.verify(modelMapper).map(associates, AssociatesDto.class);
		Mockito.verify(associatesRepository).save(associates);
	}
	@Test
	void testupdatePracticeFail() {
		assertThrows(AssociatesException.class, ()->associatesServiceImpl.updateAssociate(null));
	}
	@Test
	void testViewSucess() {
		List<Associates> list=Arrays.asList(associates);
		Mockito.when(associatesRepository.findAllByGender("female")).thenReturn(list);
		List<AssociatesDto> result=associatesServiceImpl.viewAssociates("female");
		assertEquals(list.size(), result.size());
		Mockito.verify(associatesRepository).findAllByGender("female");
		
	}
	@Test
	void testDeleteSucess() {
		associatesServiceImpl.deleteAssociate(1);
		Mockito.verify(associatesRepository).deleteById(1);
	}
}
