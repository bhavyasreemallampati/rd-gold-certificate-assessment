package com.epam;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.controller.AssociateController;
import com.epam.dto.AssociatesDto;
import com.epam.entity.Batch;
import com.epam.service.AssociatesService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@WebMvcTest(AssociateController.class)
 class AssociatesControllerTest {

	@MockBean
	AssociatesService associatesService;
	@Autowired
	MockMvc mockMvc;

	 AssociatesDto associatesDto;
	 Batch batch;

	@BeforeEach
	public void setUp() {
		batch=new Batch(1,"javafolks","java",null,null);
		associatesDto = new AssociatesDto(1,"bhavya", "bhvaya@gmail.com","female","vignan","active",batch);
	}
	
	@Test
	void testAddMentee() throws JsonProcessingException, Exception {
		Mockito.when(associatesService.addAssociate(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(post("/associates")
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associatesDto))
				.with(csrf()))
		.andExpect(status().isCreated());
	}
	@Test
	void testupdateMentee() throws JsonProcessingException, Exception {
		Mockito.when(associatesService.updateAssociate(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(put("/associates")
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associatesDto))
				.with(csrf()))
		.andExpect(status().isOk());
	}
	@Test
	void testViewMentees() throws Exception {
		List<AssociatesDto> list=Arrays.asList(associatesDto);
		Mockito.when(associatesService.viewAssociates("female")).thenReturn(list);
		mockMvc.perform(get("/associates/female")).andExpect(status().isOk());
	}
	@Test
	void testDelete() throws Exception {
		Mockito.doNothing().when(associatesService).deleteAssociate(1);
		mockMvc.perform(delete("/associates/1")).andExpect(status().isNoContent());
	}
	@Test
 void testAddAssociateWithMethodArgumentNotValidException() throws JsonProcessingException, Exception {
		AssociatesDto associatesDto = new AssociatesDto();
		Mockito.when(associatesService.addAssociate(associatesDto)).thenReturn(associatesDto);
		mockMvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(associatesDto)))
		.andExpect(status().isBadRequest());
}
	@Test
	void runtimeException() throws JsonProcessingException, Exception {
		Mockito.when(associatesService.addAssociate(any())).thenThrow(RuntimeException.class);
		mockMvc.perform(post("/associates").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(null)).with(csrf())).andExpect(status().isBadRequest());
	}


}
